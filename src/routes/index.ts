import { Router, Request, Response } from 'express'
import { auth } from '../app/middlewares/auth'
import authRouter from './auth'
import userRouter from './user'
import pratosRouter from './pratos'

const routes = Router()

routes.get('/', async (req: Request, res: Response) => {
  return res.status(200).json({ api: 'Sarraceno API' })
})

routes.use('/auth', authRouter)
routes.use('/pratos', pratosRouter)

routes.use(auth)

routes.use('/user', userRouter)

export default routes
