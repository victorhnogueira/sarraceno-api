import { Router } from 'express'
import UserController from '@src/app/controller/UserController'

const userRouter = Router()

userRouter.get('/', UserController.getUserData)
userRouter.patch('/', UserController.updateUserData)

export default userRouter
