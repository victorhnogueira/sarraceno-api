import { Router } from 'express'
import PratosController from '@src/app/controller/PratosController'

const pratosRouter = Router()

pratosRouter.get('/', PratosController.getAll)

export default pratosRouter
