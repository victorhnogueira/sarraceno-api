import { Router } from 'express'
import AuthController from '@src/app/controller/AuthController'

const authRouter = Router()

authRouter.post('/login', AuthController.userLogin)
authRouter.post('/register', AuthController.userRegister)

export default authRouter
