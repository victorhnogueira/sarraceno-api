import './util/module-alias'
import 'reflect-metadata'
import dotenv from 'dotenv'
import { SetupServer } from './app'

enum ExitStatus {
  Failure = 1,
  Success = 0
}

dotenv.config()

process.on('unhandledRejection', (reason, promise) => {
  console.log(
    `App exiting due to an unhandled promise: ${promise} and reason: ${reason}`
  )

  throw reason
})

process.on('uncaughtException', error => {
  console.log(`App exiting due to an ancaught exception: ${error}`)
  process.exit(ExitStatus.Failure)
})

//
;(async () => {
  try {
    const server = new SetupServer()
    await server.init()
    server.start()

    const ExitSignals: NodeJS.Signals[] = ['SIGINT', 'SIGTERM', 'SIGQUIT']

    for (const signal of ExitSignals) {
      process.on(signal, async () => {
        try {
          console.log(`App exited successfully`)
          await server.close()
          process.exit(ExitStatus.Success)
        } catch (error) {
          console.log(`App exited with error: ${error}`)
          process.exit(ExitStatus.Failure)
        }
      })
    }
  } catch (error) {
    console.log(`App exited with error: ${error}`)
    process.exit(ExitStatus.Failure)
  }
})()
