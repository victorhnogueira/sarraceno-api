import { Request, Response } from 'express'
import { getRepository } from 'typeorm'
import jwt from 'jsonwebtoken'
import { validate } from 'class-validator'
import User from '@entity/User'
import Prato from '../entity/Prato'

interface JwtToken {
  id: number
  iat: number
  exp: number
}

type DecodedJwtToken = JwtToken | null

class NewsController {
  async getAll(req: Request, res: Response) {
    const repository = getRepository(Prato)
    const pratos = await repository.find()

    return res.status(200).json(pratos)
  }

  // ==============================================
}

export default new NewsController()
