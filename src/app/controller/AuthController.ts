import { Request, Response } from 'express'
import { getRepository } from 'typeorm'
import bcrypt from 'bcryptjs'
import jwt from 'jsonwebtoken'
import { validate } from 'class-validator'
import User from '@entity/User'
import config, { IConfig } from 'config'

class AuthController {
  async userLogin(req: Request, res: Response) {
    const { email, password } = req.body
    const repository = getRepository(User)

    if (!email) {
      return res.status(400).json({ code: 400, error: 'E-mail not provided' })
    }

    if (!password) {
      return res.status(400).json({ code: 400, error: 'Password not provided' })
    }

    const user = await repository
      .createQueryBuilder('user')
      .addSelect('user.passhash')
      .where('user.email = :email', { email })
      .getOne()

    if (!user) {
      return res.status(404).json({ code: 404, error: 'user not found' })
    }

    const hash = `${user.passhash}`

    const isPasswordCorrect = await bcrypt.compare(password, hash)

    const jwtConfig: IConfig = config.get('App.JWTConfig')

    if (isPasswordCorrect) {
      const token = jwt.sign(
        { id: user.id },
        String(jwtConfig.get('app_secret')),
        {
          expiresIn: '1d'
        }
      )

      return res.status(200).json({ token })
    } else {
      return res.status(404).json({ code: 404, error: 'Wrong password' })
    }
  }

  // ==============================================================================

  async userRegister(req: Request, res: Response) {
    const { fullName, email, password } = req.body
    const userRepository = getRepository(User)

    if (!fullName) {
      return res
        .status(400)
        .json({ code: 400, error: 'no user fullName provided' })
    }

    if (!email) {
      return res
        .status(400)
        .json({ code: 400, error: 'no user email provided' })
    }

    if (!password) {
      return res
        .status(400)
        .json({ code: 400, error: 'no user password provided' })
    }

    const userEmailAvailable = await userRepository.findOne({
      where: { email: email }
    })

    if (userEmailAvailable) {
      return res
        .status(400)
        .json({ code: 400, error: 'E-mail for this user is already taken' })
    }

    const passhash = await bcrypt.hash(password, 8)

    const newUser = userRepository.create({
      fullName: fullName,
      email: email,
      passhash
    })

    const userErrors = await validate(newUser)

    if (userErrors.length > 0) {
      return res.status(400).json(userErrors)
    }

    try {
      await userRepository.save(newUser)
    } catch (error) {
      return res.status(400).json(error)
    }

    const jwtConfig: IConfig = config.get('App.JWTConfig')

    const token = jwt.sign(
      { id: newUser.id },
      String(jwtConfig.get('app_secret')),
      {
        expiresIn: '1d'
      }
    )

    delete newUser.passhash

    return res.status(200).json({ user: newUser, token })
  }
}

export default new AuthController()
