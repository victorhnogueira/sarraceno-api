import { Entity, Column } from 'typeorm'
import CoreEntity from './CoreEntity'
import { IsString, IsOptional, IsNumber, IsBoolean } from 'class-validator'

@Entity('pratos')
export default class Prato extends CoreEntity {
  @Column()
  @IsString()
  name!: string

  @Column()
  @IsString()
  description?: string

  @Column({ nullable: true })
  @IsOptional()
  @IsString()
  image?: string

  @Column()
  @IsNumber()
  price?: number

  @Column()
  @IsNumber()
  promotionalPrice?: number

  @Column()
  @IsBoolean()
  promotionEnabled?: boolean
}
