import {
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  BeforeInsert,
  BeforeUpdate
} from 'typeorm'

export default abstract class CoreEntity {
  @PrimaryGeneratedColumn()
  id!: number

  @CreateDateColumn({
    name: 'created_at',
    type: 'timestamp'
  })
  createdAt!: Date

  @UpdateDateColumn({
    name: 'updated_at',
    type: 'timestamp'
  })
  updatedAt!: Date

  @BeforeInsert()
  updateCreatedAt(): void {
    this.createdAt = new Date()
    this.updatedAt = new Date()
  }

  @BeforeUpdate()
  updateUpdatedAt(): void {
    this.updatedAt = new Date()
  }
}
