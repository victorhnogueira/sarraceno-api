import express, { Application } from 'express'
import bodyParser from 'body-parser'
import cors from 'cors'
import routes from './routes'
import * as database from './database/setup'

export class SetupServer {
  private app!: Application
  private PORT!: number

  constructor() {
    this.PORT = Number(process.env.PORT)
  }

  public async init(): Promise<void> {
    this.app = express()
    this.middleares()
    this.routes()
    await this.databaseSetup()
  }

  private middleares(): void {
    this.app.use(bodyParser.json())
    this.app.use(bodyParser.urlencoded({ extended: false }))
    this.app.use(cors())
  }

  private routes(): void {
    this.app.use(routes)
  }

  private async databaseSetup(): Promise<void> {
    await database.connect()
  }

  public getApp(): Application {
    return this.app
  }

  public start(): void {
    this.app.listen(this.PORT, () => {
      console.log(`Server listening on port: ${this.PORT}`)
    })
  }

  public async close(): Promise<void> {
    await database.close()
  }
}
