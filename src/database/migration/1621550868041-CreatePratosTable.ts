import { MigrationInterface, QueryRunner, Table } from 'typeorm'

export class CreatePratosTable1621550868041 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'pratos',
        columns: [
          {
            name: 'id',
            type: 'int',
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment'
          },
          {
            name: 'name',
            type: 'varchar'
          },
          {
            name: 'description',
            type: 'varchar'
          },
          {
            name: 'image',
            type: 'varchar',
            isNullable: true
          },
          {
            name: 'price',
            type: 'int'
          },
          {
            name: 'promotionalPrice',
            type: 'int'
          },
          {
            name: 'promotionEnabled',
            type: 'boolean'
          },
          {
            name: 'created_at',
            type: 'timestamp',
            default: 'NOW()'
          },
          {
            name: 'updated_at',
            type: 'timestamp',
            default: 'NOW()'
          }
        ]
      })
    )
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('pratos')
  }
}
