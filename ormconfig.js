const dotenv = require('dotenv')
dotenv.config()

const config = require('config')

const ORMConfig = config.get('App.ORMConfig')

module.exports = {
  type: process.env.DATABASE_TYPE,
  host: process.env.DATABASE_HOST,
  port: Number(process.env.DATABASE_PORT),
  username: process.env.DATABASE_USERNAME,
  password: process.env.DATABASE_PASSWORD,
  database: process.env.DATABASE_NAME,
  synchronize: ORMConfig.get('synchronize') === 'true',
  logging: ORMConfig.get('logging') === 'true',
  entities: [ORMConfig.get('entities')],
  migrations: [ORMConfig.get('migrations')],
  subscribers: [ORMConfig.get('subscribers')],
  ssl: true,
  extra: ORMConfig.get('extra'),
  cli: {
    entitiesDir: ORMConfig.get('cli.entitiesDir'),
    migrationsDir: ORMConfig.get('cli.migrationsDir'),
    subscribersDir: ORMConfig.get('cli.subscribersDir')
  }
}
